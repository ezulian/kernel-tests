# storage/block/bz2131929_STABLE_WRITES

Storage: performance regressions with STABLE_WRITES

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
